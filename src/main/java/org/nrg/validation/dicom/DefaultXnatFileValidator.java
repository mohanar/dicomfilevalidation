package org.nrg.validation.dicom;

import java.io.File;
import java.io.IOException;

public class DefaultXnatFileValidator implements XnatDicomValidator{
	  public boolean isValid(String pathToDicomFiles) {
		  boolean hasCompleteDicomBeenRecieved = false;
		  try {
			  if (pathToDicomFiles.endsWith(File.separator)) {
				  pathToDicomFiles.substring(0, pathToDicomFiles.length()-1);
			  }
			  Runtime runtime = Runtime.getRuntime();       
			  String commandCall="dcmdump --search \"7fe0,0010\" -M -dc -L -E  " + pathToDicomFiles + "/*.dcm" +  "> /dev/null";  
			  System.out.println(commandCall);
			  Process p = runtime.exec(new String[]{"sh","-c",commandCall });
			  int exitStatus = p.waitFor();
			  if (exitStatus==0) {
				  hasCompleteDicomBeenRecieved = true;
			  }
				  
		  }catch(IOException ioe) {
			  System.out.println("Encountered IOException ");
			  ioe.printStackTrace();
		  }catch(InterruptedException ie) {
			  System.out.println("Process Interrupted ");
			  ie.printStackTrace();
		  }catch(NullPointerException npe){
			  System.out.println("Null Pointer Exception " );
			  npe.printStackTrace();
		  }
		  return hasCompleteDicomBeenRecieved;
	  }

}
